# Instruction

## Project Description
This project is to sense temperature using BME280 sensor and Arduino Uno.

---
## Connection Diagram

BME280 Sensor:

![BME280](image/gy-bme280.jpg)

Arduino Uno:

![Uno](image/arduino.png)

Connection Diagram:

![Connection](image/bme-arduino.png)